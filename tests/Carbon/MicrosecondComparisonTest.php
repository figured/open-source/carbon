<?php
namespace Tests\Carbon;

use Carbon\Carbon;
use Tests\AbstractTestCase;

class MicrosecondComparisonTest extends AbstractTestCase
{
    public function testThatTwoDatesAreEqualRegardlessOfTheirMicroseconds()
    {
        $dateA = Carbon::create(2019, 1, 1)->setTime(0, 0, 0, 111);
        $dateB = $dateA->copy()->setTime(0, 0, 0, 222);

        $this->assertTrue($dateA->equalTo($dateB));
        $this->assertTrue($dateB->equalTo($dateA));
        $this->assertTrue($dateA->eq($dateB));
        $this->assertTrue($dateB->eq($dateA));
        $this->assertTrue($dateB->eq($dateB));
        $this->assertTrue($dateA->eq($dateA));
        $this->assertFalse($dateA->notEqualTo($dateB));
        $this->assertFalse($dateB->notEqualTo($dateA));
        $this->assertFalse($dateA->ne($dateB));
        $this->assertFalse($dateB->ne($dateA));
    }

    public function testThatADateIsNotGreaterThanAnotherDateIfOnlyMicrosecondsAreDifferent()
    {
        $dateA = Carbon::create(2019, 1, 1)->setTime(0, 0, 0, 111);
        $dateB = $dateA->copy()->setTime(0, 0, 0, 222);

        $this->assertFalse($dateB->greaterThan($dateA));
        $this->assertFalse($dateA->greaterThan($dateB));
        $this->assertFalse($dateB->gt($dateA));
        $this->assertFalse($dateA->gt($dateB));

        $this->assertTrue($dateB->greaterThanOrEqualTo($dateA));
        $this->assertTrue($dateA->greaterThanOrEqualTo($dateB));
        $this->assertTrue($dateB->gte($dateA));
        $this->assertTrue($dateA->gte($dateB));
    }

    public function testThatADateIsNotLessThanAnotherDateIfOnlyMicrosecondsAreDifferent()
    {
        $dateA = Carbon::create(2019, 1, 1)->setTime(0, 0, 0, 111);
        $dateB = $dateA->copy()->setTime(0, 0, 0, 222);

        $this->assertFalse($dateB->lessThan($dateA));
        $this->assertFalse($dateA->lessThan($dateB));
        $this->assertFalse($dateB->lt($dateA));
        $this->assertFalse($dateA->lt($dateB));

        $this->assertTrue($dateB->lessThanOrEqualTo($dateA));
        $this->assertTrue($dateA->lessThanOrEqualTo($dateB));
        $this->assertTrue($dateB->lte($dateA));
        $this->assertTrue($dateA->lte($dateB));
    }
}